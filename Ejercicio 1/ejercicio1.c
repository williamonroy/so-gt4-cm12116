/*IMPORTAMOS LAS FUNCIONES DE LA BIBLIOTECA ESTANDAR*/
#include <stdio.h>
/*PUNTO DE INICIO DEL PROGRAMA*/
int main()
{
    float n1;
    float n2;
    float *p1;
    float *p2;
    n1 = 4.0;
    p1 = &n1;
    p2 = p1;
    n2 = *p2;
    n1 = *p1 + *p2;
/*CON LA FUNCIÓN PRINTF MOSTRAMOS LOS VALORES DE N1 Y N2*/
    printf("n1 tiene un valor de:%d\nn2 tiene un valor de:%d\n", n1, n2);
/*FINALIZAMOS EL PROGRAMA*/
    return 0;
}
