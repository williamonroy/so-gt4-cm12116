/*IMPORTAMOS LAS FUNCIONES DE LA BIBLIOTECA ESTANDAR*/
#include <stdio.h>
/*PUNTO DE INICIO DEL PROGRAMA*/
int main()
{
/*AGREGAMOS EL CODIGO CONSIDERADO EN LA GUIA*/
    int *pnumero;
    int num1, num2;
    char *pchar;
    char letra1;
    num1 = 2;
    num2 = 5;
    letra1 = 'a';
/*HACEMOS LA ASIGNACION PARA CONOCER EL VALOR DE PNUMERO*/
    pnumero = &num1;
/*MOSTRAMOS EN PANTALLA CON LA FUNCION PRINTF*/
    printf("El valor del puntero pnumero de acuerdo a la asignación es:%d \n", *pnumero);
/*FINALIZAMOS EL PROGRAMA*/
    return 0;
}
