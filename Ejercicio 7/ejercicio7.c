#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
int dimension;
do
 {
printf("¿De que grado quiere el polinomio?:");
scanf("%d", &dimension);
}
while (dimension <= 0);
dimension++;

double *coeficientes = (double *)malloc(dimension * (sizeof(double)));

printf("Agrege los coeficientes de derecha hacia izquierda.\n");

for (int i = 0; i < dimension; i++)
{
printf("Coeficiente #%d: ", i + 1);
scanf("%lf", coeficientes);
coeficientes++;
}
   
double x;
printf("¿Que valor desea evaluar con el polinomio?");
scanf("%lf", &x);
coeficientes--;

double sumatoria;
sumatoria = sumatoria + *coeficientes;
coeficientes--;


for (double i=1; i < dimension; i++)
{
sumatoria = sumatoria + (*coeficientes * pow(x, i));
coeficientes--;
}

coeficientes++;
dimension--;
printf("P(x)=");

for (int i= 0; i< dimension; i++)
{
printf("%lf(X^%d)+", *coeficientes, x, (dimension - i));
coeficientes++;
}

printf("%lf\n", *coeficientes);
printf("P(%lf)=%lf\n", x, sumatoria);
}
