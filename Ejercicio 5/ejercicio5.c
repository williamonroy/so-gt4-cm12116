/*IMPORTAMOS LAS FUNCIONES DE LA BIBLIOTECA ESTANDAR*/
#include <stdio.h>
/*IMPORTAMOS LA BIBLIOTECA ESTANDAR PARA LA GESTION DE LA MEMORIA DINAMICA*/
#include <stdlib.h>  
/*PUNTO DE INICIO DEL PROGRAMA*/
int main()
{
/*ASIGNAMOS LA MEMORIA DINAMICA EN EL PUNTERO NUMS*/
    int *Nums = (int *)malloc(100 * sizeof(int));
/*CON EL CICLO FOR ASIGNAMOS LAS INTERACIONES DE 1 HASTA 100*/
    for (int i = 1; i <= 100; i++)
    {
/*ASIGNAMOS UNA VARIABLE NUMS Y USAMOS LA FUNCION RAND()*/

/*LA FUNCION RAND() NOS DEVOLVERA NUMEROS ALEATORIOS ENTRE 0 Y 100*/
        *Nums = rand() % 100;
/*ASIGNAMOS CUANTAS FILAS Y COLUMNAS DEBEN APARECERNOS EN CONSOLA*/
        if (i % 10 == 0)
        {
            printf("%d\n", *Nums);
        }
        else
        {
            printf("%d--", *Nums);
        }
        Nums++;
    }
}
