#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *IngresarTexto();
void contarVocales(char *, int[]);
void mostrar(int[]);

void main()
{
char *Palabra;
int num[5];
Palabra = IngresarTexto();
contarVocales(Palabra, num);
mostrar(num);
}

char *IngresarTexto()
{
char *Palabra = (char *)malloc(100 * sizeof(char));
printf("Por favor ingresa la consola la palabra o frase: ");
scanf("%[^\n]", Palabra);
return Palabra;
}

void contarVocales(char *puntero, int datos[])
{
int a = 0;
int e = 0;
int i = 0;
int o = 0;
int u = 0;

for (int x = 0; x < strlen(puntero); x++)
    {
        if (puntero[x] == 'a' || puntero[x] == 'A')
        {
            a = a + 1;
        }
        else if (puntero[x] == 'e' || puntero[x] == 'E')
        {
            e = e + 1;
        }
        else if (puntero[x] == 'i' || puntero[x] == 'I')
        {
            i = i + 1;
        }
        else if (puntero[x] == 'o' || puntero[x] == 'O')
        {
            o = o + 1;
        }
        else if (puntero[x] == 'u' || puntero[x] == 'U')
        {
            u = u + 1;
        }
    }
    datos[0] = a;
    datos[1] = e;
    datos[2] = i;
    datos[3] = o;
    datos[4] = u;
}

void mostrar(int vocales[])
{
    printf("Las vocales encontras son las siguientes:\n");
    printf("1) En la vocal a, se encontraron: %d\n", vocales[0]);
    printf("2) En la vocal e, se encontraron: %d\n", vocales[1]);
    printf("3) En la vocal i, se encontraron: %d\n", vocales[2]);
    printf("4) En la vocal o, se encontraron: %d\n", vocales[3]);
    printf("5) En la vocal u, se encontraron: %d\n", vocales[4]);
}
