/*INCLUIMOS LA BIBLIOTECA DE CABECERA ESTANDAR DE C*/
#include <stdio.h>

/*INLUIMOS LA BIBLIOTECA PARA LA GESTION DE MEMORIA DINAMICA*/
#include <stdlib.h>

/*INCLUIMOS UN MODULO PARA MAYOR*/
void mayor(float *memoriad, int dimension);

/*INCLUIMOS UN MODULO PARA MENOR*/
void menor(float *memoriad, int dimension);

/*INCLUIMOS UN MODULO PARA PRMEDIO*/
void promedio(float *memoriad, int dimension);

/*ALGORITMO PRINCIPAL DEL PROGRAMA*/
int main()
{
/*DEFINIMOS UNA VARIABLE TIPO ENTERO*/
int asignandoproporcion;

/*DEFINIMOS UN BUCLE CON UNA SENTECIA do-while*/    


/*PEDIMOS AL USUARIO QUE INGRESE LOS NUMEROS*/		
do
    {
        printf("Asigne con cuantos valores quiere operar el programa: ");
        scanf("%d", &asignandoproporcion);
    }
while (asignandoproporcion <= 0);


/*ASIGNAMOS CON MALLOC EL TAMAÑO EN MEMORIA DINAMICA*/
/*CREAMOS UN PUNTERO DE TIPO FLOAT*/
float *valores = (float *)malloc(asignandoproporcion * sizeof(float));

/*CON EL CICLO FOR ASIGNAMOS UN CICLO REPETITIVO*/
/*EL TAMAÑO DEPENDERA DE LOS NUMEROS INGRESADOS ANTERIORMENTE EN do-while*/
for (int i = 0; i < asignandoproporcion; i++)
    {
        printf("Agrege el valor N°%d a la operación:", (i + 1));
        scanf("%f", valores);
        valores++;
    }
    valores--;
    mayor(valores, asignandoproporcion);
    menor(valores, asignandoproporcion);
    promedio(valores, asignandoproporcion);
}


/*ENCONTRAMOS EL VALOR MAXIMO DE LOS NUMEROS*/
void mayor(float *memoriad, int dimension)
{
    float Valormayor = *memoriad;
    for (int i = 0; i < dimension; i++)
    {
        if (Valormayor < *memoriad)
        {
            Valormayor = *memoriad;
        }
        memoriad--;
    }
    printf("El valor mayor de los ingresados es: %f\n", Valormayor);
    return;
}

/*ENCONTRAMOS EL VALOR MINIMO DE LOS NUMEROS*/
void menor(float *memoriad, int dimension)
{
    float ValorMenor = *memoriad;
    for (int i = 0; i < dimension; i++)
    {
        if (ValorMenor > *memoriad)
        {
            ValorMenor = *memoriad;
        }
        memoriad--;
    }
    printf("El valor minimo de los ingresados es: %f\n", ValorMenor);
}
/*ENCONTRAMOS EL VALOR MEDIO DE LOS NUMEROS*/
void promedio(float *memoriad, int dimension)
{
    float Sumatoria = 0;
    for (int i = 0; i < dimension; i++)
    {
        Sumatoria = Sumatoria + *memoriad;
        memoriad--;
    }
    float promedio = Sumatoria / dimension;
    printf("El valor promedio de los ingresados es: %f\n", promedio);
}
